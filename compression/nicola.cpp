#include <stdio.h>
#include <stdint.h>
#include <math.h>





#define SMP_WORD_NO			64								// number of samples per adc channel
#define ADC_CH_NO			1								// number of adc channels per adc board
//#define L2_HEADER			2								// 2 byte words, 2 L2 headers
//#define ADC_HEADER			6								// 2 byte words. 6 ADC headers
#define PKT_HEADER_WORD_NO	8					// 2 byte words
#define ENERGY_WORD_NO			SMP_WORD_NO * ADC_CH_NO						// 2 byte words
#define COE_WORD_NO			6								// 2 byte words
#define PKT_PAYLOAD			2 * (PKT_HEADER_WORD_NO + ENERGY_WORD_NO + COE_WORD_NO)		// number of bytes per packet
#define PKT_PER_EVT         1
#define COMP_BLK_NO			PKT_PER_EVT * ADC_CH_NO
#define COMP_EVT_ELEMENT_NO		(PKT_PER_EVT * PKT_PAYLOAD + COMP_BLK_NO * 3)



// Nikola's compression (used online in May 2013)
int compress(uint16_t *data16_1, uint8_t *data8)
{

	uint16_t *d16_pointer1 = data16_1;
	uint8_t *d8_pointer = data8;
	int data8Bytes = 0;


	uint16_t min, max;
	for (int p = 0; p < PKT_PER_EVT; p++)
	{
        printf("PKT_PAYLOAD %d\n", PKT_PAYLOAD);
        printf("COMP_EVT_ELEMENT_NO %d\n", COMP_EVT_ELEMENT_NO);
		d16_pointer1 = data16_1 + p * (PKT_PAYLOAD / 2); // this puts the pointer at the beggining of the array, somehow.

		// swaping the headers - L2 and ADC
		for (int h = 0; h < PKT_HEADER_WORD_NO; h++)
		{
            // Move the header from N elements of d16_pointer1 to 2N of d8_pointer
            printf("d16_pointer[h] = %d\n", d16_pointer1[h]);
			d8_pointer[2 * h] = (uint8_t)(d16_pointer1[h] & 0x00FF);
			d8_pointer[2 * h + 1] = (uint8_t)(d16_pointer1[h] >> 8);
		}
        printf("\nheader:\n");
        for (int i = 0; i < PKT_HEADER_WORD_NO * 2; i++){ printf("d8_pointer[%d] = %d\n", i, d8_pointer[i]); }

		d8_pointer += PKT_HEADER_WORD_NO * 2;
		data8Bytes += PKT_HEADER_WORD_NO * 2;

        int bitbox = 0;
		int compressed = 0;

		// find the min and max from all the samples within a channel
		for (int a = 0; a < ADC_CH_NO; a++)
		{
			d16_pointer1 = data16_1 + p * (PKT_PAYLOAD / 2);
			d16_pointer1 += PKT_HEADER_WORD_NO;
			d16_pointer1 += a;

            //printf("d16_pointer[0] (skip header) = %d\n", d16_pointer1[0]);

			//uint8_t hibyte = (d16_pointer1[0] & 0xFF00) >> 8;
			//uint8_t lobyte = (d16_pointer1[0] & 0x00FF) & 0x3F;
			//d16_pointer1[0] = lobyte << 8 | hibyte;

			// set the first sample to be the min and max -> find min and max later
			min = d16_pointer1[0];
			max = d16_pointer1[0];

            //printf("d16_pointer1[0] = %d\n", d16_pointer1[0]);

            for (int s = 1; s < SMP_WORD_NO; s++)
			{
				//uint8_t hibyte = (d16_pointer1[ADC_CH_NO * s] & 0xFF00) >> 8;
				//uint8_t lobyte = (d16_pointer1[ADC_CH_NO * s] & 0x00FF) & 0x3F;
				//d16_pointer1[ADC_CH_NO * s] = lobyte << 8 | hibyte;
                //printf("d16_pointer1[ADC_CH_NO * s] = %d\n", d16_pointer1[ADC_CH_NO * s]);

				if (d16_pointer1[ADC_CH_NO * s] < min)
				{
					min = d16_pointer1[ADC_CH_NO * s];
				}
				if (d16_pointer1[ADC_CH_NO * s] > max)
				{
					max = d16_pointer1[ADC_CH_NO * s];
				}
			}
            printf("min, max = [%d, %d]\n", min, max);
            printf("max-min = %d\n", max-min);
			// start compression: 0 = uncompressed, 1 = compressed
			compressed = 0;
			for (int n = 0; n <= 8; n++)
			{
                //printf("n = %d\n", n);

				if (max - min < pow(2, n))
				{
                    bitbox = n;
                    //printf("max - min = %d\n", max - min);
                    //printf("can fit into n = %d\n", n);


					d8_pointer[0] = n;

					uint8_t hibyte = (min & 0xFF00) >> 8;
					uint8_t lobyte = (min & 0x00FF);

					d8_pointer[1] = hibyte;
					d8_pointer[2] = lobyte;
                    printf("\nn, min (bi + low):\n");
                    printf("d8_pointer[%d]= %d\n", PKT_HEADER_WORD_NO * 2 + 0, n);
                    printf("d8_pointer[%d]= %d\n", PKT_HEADER_WORD_NO * 2 + 1, hibyte);
                    printf("d8_pointer[%d]= %d\n", PKT_HEADER_WORD_NO * 2 + 2, lobyte);


					d8_pointer += 3;
					data8Bytes += 3;

					int byte = 0;
					int bit = 0;

                    printf("\nSMP_WORDs:\n");
					for (int s = 0; s < SMP_WORD_NO; s++)
					{
                        //printf("bit = [%d]\n", bit);

                        //printf("A, B, C = [%d, %d, %d]\n", (8 - n - bit), n + bit - 8, 8 - (n + bit - 8));
						if (8 - n - bit >= 0)
						{
                            //printf("n, bit = [%d, %d]\n", n, bit);
							d8_pointer[byte] += ((d16_pointer1[ADC_CH_NO * s] - min) << (8 - n - bit));
                            //printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 3 + byte, d8_pointer[byte]);

							bit += n;
						}
						else
						{
                            uint8_t byte1 = ((d16_pointer1[ADC_CH_NO * s] - min) >> (n + bit - 8));
                            uint8_t byte2 = (((d16_pointer1[ADC_CH_NO * s] - min) << (8 - (n + bit - 8))) & 0x00FF);

                            //printf("byte1, byte2 = [%d, %d]\n", byte1, byte2);

							d8_pointer[byte] += byte1;
							d8_pointer[byte + 1] += byte2;
                            printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 3 + byte, d8_pointer[byte]);

                            //printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 3 + byte, d8_pointer[byte]);
                            //printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 3 + byte + 1, d8_pointer[byte+1]);
							byte += 1;
							bit = bit + n - 8;
						}
					}

					d8_pointer += n * SMP_WORD_NO / 8;
					data8Bytes += n * SMP_WORD_NO / 8;

					compressed = 1;
					break;
				}
			}

			if (compressed == 0)
			{
                printf("\nNo compressed.\n");

				d8_pointer[0] = (uint8_t)16;

				d8_pointer += 1;
				data8Bytes += 1;

                printf("\nSMP_WORDs:.\n");
				for (int s = 0; s < SMP_WORD_NO; s++)
				{
					d8_pointer[2 * s] = (uint8_t)(d16_pointer1[ADC_CH_NO * s] >> 8);
					d8_pointer[2 * s + 1] = (uint8_t)(d16_pointer1[ADC_CH_NO * s] & 0x00FF);
                    printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 1 + 2*s, d8_pointer[2*s]);
                    printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 1 + 2*s+1, d8_pointer[2*s+1]);
				}

				d8_pointer += SMP_WORD_NO * 2;
				data8Bytes += SMP_WORD_NO * 2;
			}

		}

        printf("\nCOE_WORDs:\n");
		d16_pointer1 = data16_1 + p * (PKT_PAYLOAD / 2) + (PKT_PAYLOAD / 2) - COE_WORD_NO;
		for (int c = 0; c < COE_WORD_NO; c++)
		{
			d8_pointer[2 * c] = (uint8_t)(d16_pointer1[c] & 0x00FF);
			d8_pointer[2 * c + 1] = (uint8_t)(d16_pointer1[c] >> 8);

            if (compressed){
                printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 3 + bitbox * SMP_WORD_NO / 8 + 2 * c, d8_pointer[2 * c]);
                printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 3 + bitbox * SMP_WORD_NO / 8 + 2 * c+1, d8_pointer[2 * c+1]);
            }
            else{
                printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 1 + SMP_WORD_NO *2 + 2 * c, d8_pointer[2 * c]);
                printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 1 + SMP_WORD_NO *2 + 2 * c+1, d8_pointer[2 * c+1]);
            }
		}
		d8_pointer += COE_WORD_NO * 2;
		data8Bytes += COE_WORD_NO * 2;

	}



	return data8Bytes;

} // end Nikola's  compress()  function





int main()
{
    const int d16l = PKT_HEADER_WORD_NO + SMP_WORD_NO + COE_WORD_NO;
    const int d8l = 0;

    uint16_t data16[d16l] = {1, 1, 1, 1, 1, 1, 1, 1, 554, 553, 554, 555, 555, 556, 557, 556, 554, 554, 560, 553, 551, 553, 555, 552, 557, 553, 553, 554, 552, 556, 553, 554, 565, 606, 728, 994, 1425, 1972, 2538, 3023, 3343, 3456, 3359, 3075, 2661, 2199, 1759, 1379, 1070, 859, 724, 651, 624, 617, 613, 619, 627, 633, 641, 641, 654, 658, 663, 666, 665, 657, 648, 648, 637, 633, 622, 612, 257, 257, 257, 257, 257, 257};
    //uint16_t data16[d16l] = {1, 1, 1, 1, 1, 1, 1, 1, 461, 500, 462, 465, 463, 463, 461, 465, 464, 460, 462, 460, 457, 459, 458, 459, 462, 458, 461, 460, 461, 458, 459, 459, 459, 459, 457, 460, 461, 456, 464, 462, 462, 460, 464, 458, 464, 462, 467, 462, 463, 467, 462, 462, 463, 464, 462, 461, 461, 461, 462, 460, 457, 460, 458, 458, 457, 455, 460, 459, 458, 457, 457, 460, 257, 257, 257, 257, 257, 257};


    //printf("\nd16l = %d\n", d16l);
    for (int i = 8; i < d16l-6; i++){
        //data16[i] = 500 + data16[i]/50;
        //data16[i] = i*0.01;
    }

    //data16[30] = 30;
    uint8_t data8[COMP_EVT_ELEMENT_NO];

    for (int i = 0; i < 94; i++){
        data8[i] = 0;
    }

    int result = compress(&data16[0], &data8[0]);

    //for (int i = 0; i < result; i++){
    //    printf("data8[%d]= %d\n", i, data8[i]);
    //}

    printf("uncompress bytes = %d\n", d16l*2);
    printf("compress bytes = %d\n", result);
    printf("compression factor = %f\n", (float)d16l*2/result);




    // Uncompress
    uint16_t data16_uncompressed[d16l];

    //for(int i=19;i<66;i++){
    //    printf("data8[%d]= %d\n", i, data8[i]);
    //}

    int r = data8[16];
    uint16_t min = (data8[17] & 0x00FF) << 8 | data8[18] & 0x00FF;
    int i = 19; // Where the SMP data starts in the 8 bit array


    int bit = 0;
    int byte = 0;

    if (r > 8)
    {
        i = i-2;
        while (byte < 64){
            data16_uncompressed[byte] = ((data8[i] << 8) | (data8[i+1]));
            i += 2;
            byte++;
        }
    }
    else
    {
        printf("r: %d; min: %d\n", r, min);

        while (byte < 64){
            //printf("byte=%d  %d\n", byte, data8[i]);
            if (8 - r - bit >= 0) // if the element fits into this byte
            {
                data16_uncompressed[byte] = ((data8[i] >> (8 - r - bit)) & ((1 << r)-1)) + min;
                bit += r;
                byte ++;
                //printf("buuuuu\n");
            }
            else
            {
                i ++;
                data16_uncompressed[byte] = ((data8[i-1] & ((1 << (8-bit))-1)) << (r + bit - 8) | data8[i] >> (8 - (r + bit - 8))) + min;
                byte ++;
                bit = bit + r - 8;
                //printf("buuuuu %d\n", bit);
            }
        }
    }

    printf("\nComprobation:\n");
    for(int i=0;i<64;i++){
        //printf("data16_uncompressed[%d]= %d\n", i, data16_uncompressed[i]);
        if (data16_uncompressed[i] != data16[i+8]){
            printf("i=%d; unComp=%d; comp = %d\n", i, data16_uncompressed[i], data16[i+8]);
        }
    }
    printf("  -> All done\n");

}
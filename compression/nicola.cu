#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <chrono>
#include <thread>
#include <fstream>
#include <string>

#define NUM_BLOCKS 5096 // max 65535

#define THREADS_PER_BLOCK 2
#define COMPRESS_OLD 0


inline void CHECK(const cudaError_t error)
{
    if(error != cudaSuccess)
    {
        fprintf(stderr, "Error: %s:%d, ", __FILE__, __LINE__);
        fprintf(stderr, "code: %d, reason: %s\n", error, cudaGetErrorString(error));
        exit(1);
    }
}

std::chrono::steady_clock::time_point cpuTimer()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    //printf("buu : \t\t%f:  \n", ((double)tp.tv_sec*1e6 + (double)tp.tv_usec));


    std::chrono::steady_clock::time_point t = std::chrono::steady_clock::now();
    return (t);
    //return (0);
}


__global__ void compress(u_int16_t *d_idata, uint8_t *d_odata, unsigned int wfmLen, unsigned int nWfms,  u_int8_t *bytesPerWfm)
{
    // set thread ID
    unsigned int tid = threadIdx.x; // inside block
    //unsigned int idx = blockIdx.x*blockDim.x+threadIdx.x;
	//unsigned int offset = 0;

	//__shared__ int cacheMax[64]; // block size, for the moment
	//__shared__ int cacheMin[64]; // block size, for the moment


    // boundary check
    if(blockIdx.x >= nWfms) return;
    if(tid >= wfmLen) return;

    __shared__ int minMax[2];
    //__shared__ int waveform[64];
    //__shared__ int mutex;
    __shared__ int compressedWfm[64*2 + 4];
    __shared__ int r;
    //__shared__ bool compressed;
    
    // the team
    //printf("blockIdx.x: %d  idx: %d  tid: %d\n", blockIdx.x, idx, tid);
    
    //if (tid == 0){
    //}

    // Starting pointer for every block
    u_int16_t *idata = d_idata + blockIdx.x*wfmLen; // TOOD: make this shared
    //u_int8_t *odata = d_odata + blockIdx.x*n; // WORKS ONLY FOR ONE WFM
    //waveform[tid] = idata[tid];
    int idataTid = idata[tid];



//	__syncthreads();
//
//	// reduction
//
//    int tid2 = tid-wfmLen/2;
//    // First iteration
//    if(tid<wfmLen/2){
//        cacheMax[tid] = max(idata[tid], idata[tid + wfmLen/2]);
//    }
//    else if (tid2 >= 0 && tid2 < wfmLen/2){
//        cacheMin[tid2] = min(idata[tid2], idata[tid2 + wfmLen/2]);
//    }   
//
//    for(int stride=wfmLen/4; stride>0; stride>>=1)
//    {
//        if(tid<stride){
//            cacheMax[tid] = max(cacheMax[tid], cacheMax[tid + stride]);
//        }
//        else if (tid2 >= 0 && tid2 < stride){
//            cacheMin[tid2] = min(cacheMin[tid2], cacheMin[tid2 + stride]);
//        }
//        __syncthreads();
//    }
//
//    if(tid == 0){
//        minMax[1] = max(cacheMax[0], cacheMax[1]);
//        minMax[0] = min(cacheMin[0], cacheMin[wfmLen-1]);
//    }
//    //else if (tid==1)
//
//	__syncthreads();


//    if (tid == 0){
//
//        int min = 65536;
//        for(int i = 0; i < wfmLen; i += 1)
//        {
//            if (idata[i] < min)
//                min = idata[i];
//        }
//        minMax[0] = min;
//    }
//    else if (tid == 1){
//        int max = 0;
//        for(int i = 0; i < wfmLen; i += 1)
//        {
//            if (idata[i] > max)
//                max = idata[i];
//        }     
//        minMax[1] = max;
//    }



    atomicMax(&minMax[1], idataTid);
    atomicMin(&minMax[0], idataTid);


    //if (tid == 1){
    //    compressed = 1;
    //    r = 16;
    //}


    //else if (tid == 0){
    //    atomicMax(&minMax[1], idata[0]);
    //    atomicMin(&minMax[0], idata[0]);
    //}
    


    //__syncthreads();
    
    //__syncthreads();
    //if (tid==0){
        //minMax[0] = minT;
        //minMax[1] = maxT;

    //    printf("blockID: %d; max=%d\n",blockIdx.x,  minMax[1]);
    //    printf("blockID: %d; cache[0]=%d\n",blockIdx.x, cache[0]);
    //    printf("min: %d; max=%d\n",minMax[0],  minMax[1]);
    //    printf("blockID: %d; cacheMin[-1]=%d\n",blockIdx.x, cacheMin[wfmLen-1]);
    //}



    __syncthreads();


    //r = 32 - __clz(minMax[1] - minMax[0]);

    r = __clz(minMax[1] - minMax[0]);
    r = (r < 8) ? r : 16;

    compressedWfm[0] = r;

    int byte = 3 + int(tid*r/8);
    int bit = tid*r % 8;
    //compressedWfm[byte] = 0;  // YOU MIGHT NEED THIS AGAIN

    __syncthreads();
    if (r != 16){
        compressedWfm[1] = (minMax[0] & 0xFF00) >> 8;
        compressedWfm[2] = (minMax[0] & 0x00FF);
        if (8 - r - bit >= 0)
        {
            //printf("n, bit = [%d, %d]\n", n, bit);
            atomicAdd(&compressedWfm[byte], ((idataTid - minMax[0]) << (8 - r - bit)));
            //printf("buu %d %d\n", byte, blockIdx.x);
        }
        else
        {
            uint8_t byte1 = ((idataTid - minMax[0]) >> (r + bit - 8));
            uint8_t byte2 = (((idataTid - minMax[0]) << (8 - (r + bit - 8))) & 0x00FF);

            atomicAdd(&compressedWfm[byte],   byte1);
            atomicAdd(&compressedWfm[byte+1], byte2);
        }
    }
    if (r == 16){

        compressedWfm[1 + 2 * tid] = (uint8_t)(idataTid >> 8);
        compressedWfm[1 + 2 * tid + 1] = (uint8_t)(idataTid & 0x00FF);
        //printf("idata[%d]:%d\n", tid, idata[tid]);
    }

    __syncthreads();



    d_odata[blockIdx.x*(wfmLen*2) + blockIdx.x  + 1+tid]   = compressedWfm[1+tid];
    d_odata[blockIdx.x*(wfmLen*2) + blockIdx.x  + 64+tid+1] = compressedWfm[64 + tid+1];

    d_odata[blockIdx.x*(wfmLen*2) + blockIdx.x] = compressedWfm[0];

}




__global__ void compress_slow(uint16_t *d_idata, uint8_t *d_odata, unsigned int wfmLen, unsigned int nWfms,  uint8_t *bytesPerWfm)
{
    // set thread ID
    unsigned int tid = threadIdx.x; // inside block I guess
    unsigned int idx = blockIdx.x*blockDim.x+threadIdx.x;

    // boundary check
    if(blockIdx.x >= nWfms) return;

    __shared__ u_int16_t minMax[2];
    
    // the team
    //printf("blockIdx.x: %d  idx: %d  tid: %d\n", blockIdx.x, idx, tid);
    
    //if (tid == 0){
    //}

    // Starting pointer for every block
    u_int16_t *idata = d_idata + blockIdx.x*wfmLen;
    //u_int8_t *odata = d_odata + blockIdx.x*n; // WORKS ONLY FOR ONE WFM



    if (idx == 2){

        for(int i=0;i<wfmLen*nWfms;i++){
            //printf("buu i=%d;%d\n", i, idata[i]);
        }
    }
    __syncthreads();

    // Find min and max
    if (tid == 0){

        int min = 65536;
        int max = 0;
        for(int i = 0; i < wfmLen; i += 1)
        {
            if (idata[i] < min)
                min = idata[i];
            else if (idata[i] > max)
                max = idata[i];
            //printf("idata[%d]=%d\t :: blockIdx.x=%d \n", i, idata[i], blockIdx.x);
        }
        minMax[0] = min;
        minMax[1] = max;
    }
//    else{
//        for(int i = 0; i < wfmLen; i += 1)
//        {
//            if (idata[i] > max)
//                max = idata[i];
//        //printf("%d :: %d  %d  %d\n", tid, max, blockIdx.x, min);
//        }     
//        minMax[1] = max;
//    }

    //__syncthreads();

    //if (tid == 0){
    //    printf("blockIdx.x:%d  min: %d  max: %d\n", blockIdx.x, minMax[0], minMax[1]);
    //}

    __syncthreads();

    if (tid == 0){
        
        bool compressed = 0;
		for (u_int8_t r = 0; r <= 8; r++)
		{
            if (minMax[1] - minMax[0] < pow(2, r))
			{
                //printf("bID:%d, doing compression, max-min=%d; r=%d\n", blockIdx.x, minMax[1]-minMax[0], r);
                
                //for (int i = 8; i < wfmLen; i++){
                //    printf("idata[%d]= %d\n", i, idata[i]);
                //}

                int byte = blockIdx.x*wfmLen*2 + blockIdx.x;
                d_odata[byte+0] = r;
                d_odata[byte+1] = (minMax[0] & 0xFF00) >> 8;
                d_odata[byte+2] = (minMax[0] & 0x00FF);
                byte += 3;

                int bit = 0;
			    for (int s = 0; s < wfmLen; s++)
			    {
                    if (8 - r - bit >= 0)
                    {
                        //printf("n, bit = [%d, %d]\n", n, bit);
                        d_odata[byte] += ((idata[s] - minMax[0]) << (8 - r - bit));

                        bit += r;
                    }
                    else
                    {
                        uint8_t byte1 = ((idata[s] - minMax[0]) >> (r + bit - 8));
                        uint8_t byte2 = (((idata[s] - minMax[0]) << (8 - (r + bit - 8))) & 0x00FF);


                        d_odata[byte] += byte1;
                        d_odata[byte + 1] += byte2;

                        //printf("d8_pointer[%d] = %d\n", PKT_HEADER_WORD_NO * 2 + 3 + byte + 1, d8_pointer[byte+1]);
                        byte += 1;
                        bit = bit + r - 8;
                    }
                    //printf("k_d_odata[%d] = %d\n", byte, d_odata[byte]);
                    
               }
                compressed = 1;
                break;
            }
        }
        if (compressed == 0){
            //printf("bID:%d, not compressed\n", blockIdx.x);
            int byte = blockIdx.x*wfmLen*2 + blockIdx.x;

            d_odata[byte+0] = 16;  // r

            for (int s = 0; s < wfmLen; s++)
			{
				d_odata[(byte+1) + 2 * s] = (uint8_t)(idata[s] >> 8);
				d_odata[(byte+1) + 2 * s + 1] = (uint8_t)(idata[s] & 0x00FF);
                //printf("%d\n", (byte+1) + 2 * s + 1);
			}

        }

    }
    __syncthreads();

    //printf("min[0]: %d min[1]: %d\n", d_odata[1], d_odata[2]);


    //return;




    // in-place reduction in global memory
    //for(int stride=1;stride<blockDim.x; stride *=2)
    //{
    //    if((tid%(2*stride))==0)
    //        idata[tid] += idata[tid+stride];
    //    // synchrnoize within block
    //    __syncthreads();
    //}
    // write result for this block to global mem
    //if(tid==0)d_odata[blockIdx.x] = idata[0];
}


void getWfms(u_int16_t *wfmArray, unsigned int n)
{

    std::string s;


    //for (int wfmN=0; wfmN<n; wfmN++)
    int wfmsPerFile = 1<<10;
    int maxFileN = 2837;
    int fileN = 0; 


    while (fileN<n/wfmsPerFile)
    {
        //printf("asdasdddddeeeeeee\n");

        s = std::to_string(fileN);

        std::fstream myfile("/home/mario/data/fbar/1024/"+s+".txt", std::ios_base::in);
        

        if (fileN > maxFileN){
            printf("Reached the limit of available files.\n");
            printf("Max number of wfms is %d.\n", maxFileN*wfmsPerFile);
            exit(1);
        }

        if (fileN % 10 == 0){
            //printf("File %d \n", fileN);
        }


        if (!myfile){
            printf("file %d does not exist\n", fileN);
            continue;
        }


        int a;
        int i = 0;

        while (myfile >> a)
        {
            //printf("wfm %d \n", a);
            //if (i % 1000 == 0){
            //    printf("wfm %d \n", i);
            //}
            wfmArray[fileN*wfmsPerFile*64 + i] = a;
            i++;
        }
        //printf("sadadasdas\n");
        fileN++;

    }

}



void launchCompress(float *timingResults, int nWfms, uint16_t *wfmBuffer)
{


    //std::this_thread::sleep_for(std::chrono::milliseconds(1000));    



    int wfmLen = 64;

    //uint16_t wfmArray[wfmLen] = {554, 553, 554, 555, 555, 556, 557, 556, 554, 554, 560, 553, 551, 553, 555, 552, 557, 553, 553, 554, 552, 556, 553, 554, 565, 606, 728, 994, 1425, 1972, 2538, 3023, 3343, 3456, 3359, 3075, 2661, 2199, 1759, 1379, 1070, 859, 724, 651, 624, 617, 613, 619, 627, 633, 641, 641, 654, 658, 663, 666, 665, 657, 648, 648, 637, 633, 622, 612};
    //uint16_t wfmArray[wfmLen] = {461, 500, 462, 465, 463, 463, 461, 465, 464, 460, 462, 460, 457, 459, 458, 459, 462, 458, 461, 460, 461, 458, 459, 459, 459, 459, 457, 460, 461, 456, 464, 462, 462, 460, 464, 458, 464, 462, 467, 462, 463, 467, 462, 462, 463, 464, 462, 461, 461, 461, 462, 460, 457, 460, 458, 458, 457, 455, 460, 459, 458, 457, 457, 460};
    //uint16_t wfmArray[wfmLen*nWfms] = {466, 455, 800, 465, 463, 463, 461, 465, 464, 460, 462, 460, 457, 459, 458, 459, 462, 458, 461, 460, 461, 458, 459, 459, 459, 459, 457, 460, 461, 456, 464, 462, 462, 460, 464, 458, 464, 462, 467, 462, 463, 467, 462, 462, 463, 464, 462, 461, 461, 461, 462, 460, 457, 460, 458, 458, 457, 455, 460, 459, 458, 457, 457, 467};


    //for(int i=0;i<wfmLen*nWfms;i++){
    //    printf("buu i=%d;%d\n", i, wfmBuffer[i]);
    //}

    //exit(1);

    // allocate host memory
    size_t bytes = wfmLen*nWfms*sizeof(uint16_t);
    uint16_t *h_idata = (uint16_t *)malloc(bytes);
    uint8_t *h_odata = (uint8_t *)malloc(bytes + nWfms*sizeof(uint8_t)); // maximum uncompressed size
    


    // allocate device memory
    uint16_t *d_idata;
    uint8_t *d_odata;
    uint8_t *bytesPerWfm;
    cudaMalloc((void**)&d_idata, bytes); 
    cudaMalloc((void**)&d_odata, bytes + nWfms*sizeof(uint8_t));
    cudaMalloc((void**)&bytesPerWfm, nWfms);


    //// get wfms from files
    //getWfms(h_idata, nWfms);

    // get wfms from buffer
    for (int i=0; i < wfmLen*nWfms; i++){
        h_idata[i] = wfmBuffer[i];
    }



    // memcpy H to D
    cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice);


    // Timing settings
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);


    // Run Kernel
    cudaEventRecord(start);
    if (COMPRESS_OLD){
        compress_slow<<<nWfms, THREADS_PER_BLOCK>>>(d_idata, d_odata, wfmLen, nWfms, bytesPerWfm);
    }
    else{
        dim3 gridSize = nWfms;   // N blocks
        dim3 blockSize = 64; // Threads per block
        compress<<<gridSize, blockSize>>>(d_idata, d_odata, wfmLen, nWfms, bytesPerWfm);
    }
    cudaDeviceSynchronize();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&timingResults[0], start, stop);


    // memcpy D to H
    cudaDeviceSynchronize();
    cudaMemcpy(h_odata, d_odata, bytes+nWfms*sizeof(uint8_t), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();


    // Uncompress
    //printf("Doing Decompression\n");

    //uint16_t data16_uncompressed[nWfms * wfmLen];
    uint16_t *data16_uncompressed = (uint16_t *)malloc(bytes);


    for(int i=0;i<wfmLen*nWfms*2;i++){
        //printf("h_odata[%d] = %d\n", i, h_odata[i]);
    }


    for(int wfmN=0; wfmN<nWfms; wfmN++){

        int r = h_odata[wfmN*wfmLen*2 + wfmN];
        //printf("a: %d; b: %d; c: %d: \n", h_odata[wfmN*wfmLen*2 + wfmN-4], h_odata[wfmN*wfmLen*2 + wfmN-5], h_odata[wfmN*wfmLen*2 + wfmN+-6]);


        uint16_t min = (h_odata[wfmN*wfmLen*2 + wfmN + 1] & 0x00FF) << 8 | h_odata[wfmN*wfmLen*2 + wfmN + 2] & 0x00FF;
        int i = wfmN*wfmLen*2 + wfmN + 3; // Where the SMP data starts in the 8 bit array


        int bit = 0;
        int byte = wfmN*wfmLen;
        //printf("r: %d; min: %d; i: %d:; wfmN: %d \n", r, min, i, wfmN);

        if (r > 8)
        {
            i = i-2;
            while (byte < wfmN*wfmLen + wfmLen){
                data16_uncompressed[byte] = ((h_odata[i] << 8) | (h_odata[i+1]));
                i += 2;
                byte++;
            }
        }
        else
        {
            
            while (byte < wfmN*wfmLen + wfmLen){
                //printf("byte=%d  %d\n", byte, h_odata[i]);
                if (8 - r - bit >= 0) // if the element fits into this byte
                {
                    data16_uncompressed[byte] = ((h_odata[i] >> (8 - r - bit)) & ((1 << r)-1)) + min;
                    bit += r;
                    byte ++;
                    //printf("buuuuu\n");
                }
                else
                {
                    i ++;
                    data16_uncompressed[byte] = ((h_odata[i-1] & ((1 << (8-bit))-1)) << (r + bit - 8) | h_odata[i] >> (8 - (r + bit - 8))) + min;
                    byte ++;
                    bit = bit + r - 8;
                }
                if (data16_uncompressed[byte-1] != h_idata[byte-1]){
                    //printf("i=%d; byte=%d; unComp=%d; original = %d\n", i, byte-1, data16_uncompressed[byte-1], h_idata[byte-1]);
                }
            }
        }

        //cudaEventDestroy(start);
        //cudaEventDestroy(stop);
    }

    //printf("\nComprobation:\n");
    for(int i=0 ; i<nWfms*wfmLen ; i++){
        if (data16_uncompressed[i] != h_idata[i]){
                printf("i=%d; unComp=%d; original = %d\n", i, data16_uncompressed[i], h_idata[i]);
            return;
        }
    }
    //printf("  -> All done\n");

    free(h_idata);
    free(h_odata);
    free(data16_uncompressed);
}

int main(void)
{

    float *timingResults = (float *)malloc(3*sizeof(float));




    int wfmLen=64;
    int wfmsFromFile = 2.5e6;
    uint16_t *wfmBuffer = (uint16_t *)malloc(wfmLen*wfmsFromFile*sizeof(uint16_t));
    getWfms(wfmBuffer, wfmsFromFile);
    printf("#nWfms   compression time\n");


    for(int i=0;i<wfmLen*wfmsFromFile;i++){
        //printf("buu i=%d;%d\n", i, wfmBuffer[i]);
    }

    //launchCompress(timingResults, 2e6, wfmBuffer);
    for(int nWfms=1e4 ; nWfms<wfmsFromFile ; nWfms*=1.2){
        launchCompress(timingResults, int(nWfms), wfmBuffer);
        printf("%d\t %f\n", nWfms, timingResults[0]*1e6f);
    }

}



// 0 1 2 3 4 5 6 7